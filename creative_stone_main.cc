#include <stdio.h>
#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <map>
#include "creative_stone.h"

using namespace std;

template <typename T>
string to_string(T value)
{
    std::ostringstream os ;
    os << value ;
    return os.str() ;
}

string infoLog(Creative_stone* MINION,int _Num) //몬스터의 정보를 알려주는 함수.
{ 
    string output = ""; 
    output += MINION->_Name(_Num);
    output += " "+ to_string(MINION->hp(_Num));
    output += " ";
    output += to_string(MINION->power(_Num)); 
    
    return output;
}

bool creative(Creative_stone* Myteam,Creative_stone* Yourteam)
{
	string inputs;  //inputs라는 입력 하는 string 선언.
  	getline(cin, inputs);

	if(inputs == ":quit") return false;  //#quit 입력하면 탈출.
  	else if(inputs.find(":add") != string::npos)      //:add oName oHp oPower 
    	{
        	inputs.erase(0, 5);  //:add 를 지움.
        
        	int oHp = -1;      //아군 체력
        	int oPower = -1;    //아군 파워
        	int number = -1;
		string oName;     //아군 이름.
		int putNumber=-1;  //이름을 다 넣었는지 확인.
        	
        	for(int i=0; i<inputs.size(); ++i)
        	{
        		char c = inputs[i];
            		if(putNumber<0)
			{
				if(c!=' ')
				{
					oName+=c;   
				}
				else
					putNumber=1;
			}
			else if(putNumber>0)
			{
            			if(isdigit(c) == true)
            			{
            				int num = (int)c - (int)'0';  //문자로 입력된 것들 숫자로 바꾸는 작업.
            				if(number < 0) number = num; 
            				else number = number * 10 + num;  //12 나 123 처럼 한자리 이상으로 넘어갈때를 작업 해주는 과정.
           			}
            			else
            			{
                			if(c == ' ') oHp = number; 
                				number = -1;
            			}
			}
       		}

		putNumber-=2;

		if(oHp >= 0) oPower = number;  
		Myteam->Setting(oName,oHp,oPower);  //셋팅   !!!!프린트 하는 함수 구현!!!!
		if((Myteam->indiPrint())==1)
		{
			for(int i=0; i<(Myteam->points());i++)
			{
				if((Yourteam->checkblank())==1)
				{
					if(Yourteam->_Name(i)!="")
						cout<<infoLog(Myteam,i)<<" / "<<infoLog(Yourteam,i)<<endl;
					else
						cout<<infoLog(Myteam,i)<<endl;
				}
				else
				{
					cout<<infoLog(Myteam,i)<<endl;
				}
			}
		}
		Myteam->resetPrint();
	}

	else if(inputs.find(":foeadd") != string::npos)   //:foeadd eName eHp ePower
    	{
        	inputs.erase(0, 8);  //:add 를 지움.
        
        	int eHp = -1;     //적 체력
        	int ePower = -1;    //적 파워
        	int number = -1;
		string eName;     //적군 이름.
		int putNumber2=-1;  //이름을 다 넣었는지 확인.
        	
        	for(int i=0; i<inputs.size(); ++i)
        	{
        		char c2 = inputs[i];
            		if(putNumber2<0)
			{
				if(c2!=' ')
				{
					eName+=c2;   
				}
				else
					putNumber2=1;
			}
			else if(putNumber2>0)
			{
            			if(isdigit(c2) == true)
            			{
            				int num2 = (int)c2 - (int)'0';  //문자로 입력된 것들 숫자로 바꾸는 작업.
            				if(number < 0) number = num2; 
            				else number = number * 10 + num2;  //12 나 123 처럼 한자리 이상으로 넘어갈때를 작업 해주는 과정.
           			}
            			else
            			{
                			if(c2 == ' ') eHp = number;
                			number = -1;
            			}
			}
       		}
		if(eHp >= 0) ePower = number;  
		Yourteam->Setting(eName,eHp,ePower);     //셋팅
		if((Yourteam->indiPrint())==1)
		{
			for(int i=0; i<(Myteam->points());i++)
			{
				if((Yourteam->checkblank())==1)
				{
					if(Yourteam->_Name(i)!="")
						cout<<infoLog(Myteam,i)<<" / "<<infoLog(Yourteam,i)<<endl;
					else
						cout<<infoLog(Myteam,i)<<endl;
				}
				else
				{
					cout<<infoLog(Myteam,i)<<endl;
				}
			}
		}
		Myteam->resetPrint();
	}

	else if(inputs.find(":attack") != string::npos)   //:attack oName eName
    	{
        	inputs.erase(0, 8);  //:attack 를 지움.
       
		string lName;     //아군 이름.(왼쪽)
		string rName;     //적군 이름.(오른쪽)
        
		string Names="";
        	
        	for(int i=0; i<inputs.size(); ++i)
        	{
        		char c3 = inputs[i];
            
            		if(c3!=' ')
            		{
            			Names+=c3;
           		}
            		else
            		{
                		lName=Names; 
                		Names="";
            		}
       		}
		if(Names!="") rName = Names;  //
		Myteam->attack(lName,Yourteam,rName);     //공격 !!!!!출력!!!!!
		if((Myteam->indiPrint())==1)
		{
			for(int i=0; i<(Myteam->points());i++)
			{
				if((Yourteam->checkblank())==1)
				{
					if(Yourteam->_Name(i)!="")
						cout<<infoLog(Myteam,i)<<" / "<<infoLog(Yourteam,i)<<endl;
					else
						cout<<infoLog(Myteam,i)<<endl;
				}
				else
				{
					cout<<infoLog(Myteam,i)<<endl;
				}
			}
		}
		Myteam->resetPrint();
	}

	else if(inputs.find(":burn") != string::npos)   //:Burn # or :Burn name #
    	{
        	inputs.erase(0, 6);  //:Burn 을 지움.
        
        	int Burndamage = -1;      //입히는 피해.
        	int number = -1;
		int not_only_number=-1;   //숫자만 적었는지 확인
		int putNumber3=-1;
		string letters;
		string anyName;     //이름.
        	
		for(int i=0;i<inputs.size();i++)
		{
			if(inputs[i]==' ')
				not_only_number=1;     //1이면 이름도 적은 것임.
		}
		if(not_only_number<0)  //숫자만 적었을때
		{
			
        		for(int i=0; i<inputs.size(); ++i)
        		{
        			char c4 = inputs[i];
            		
            			if(isdigit(c4) == true)
            			{
            				int num4 = (int)c4 - (int)'0';  //문자로 입력된 것들 숫자로 바꾸는 작업.
            				if(number < 0) number = num4; 
            				else number = number * 10 + num4;  //12 나 123 처럼 한자리 이상으로 넘어갈때를 작업 해주는 과정.
           			}
            			
       			}
			
			Burndamage=number;
			Myteam->burn1(Burndamage,Yourteam);
			if((Myteam->indiPrint())==1)
			{
				for(int i=0; i<(Myteam->points());i++)
				{
					if((Yourteam->checkblank())==1)
					{
						if(Yourteam->_Name(i)!="")
							cout<<infoLog(Myteam,i)<<" / "<<infoLog(Yourteam,i)<<endl;
						else
							cout<<infoLog(Myteam,i)<<endl;
					}
					else
					{
						cout<<infoLog(Myteam,i)<<endl;
					}
				}
			}
			Myteam->resetPrint();
		}
		
		else if(not_only_number>0)    //문자도 적은것.
		{
			for(int i=0; i<inputs.size(); ++i)
			{
				char c5 = inputs[i];
				if(putNumber3<0)
				{
					if(c5!=' ')
					{
						anyName+=c5;   
					}
					else
						putNumber3=1;
				}
				
				
				else if(putNumber3>0)		
        			{

            				if(isdigit(c5) == true)
            				{
            					int num5 = (int)c5 - (int)'0';  //문자로 입력된 것들 숫자로 바꾸는 작업.
            					if(number < 0) number = num5; 
            					else number = number * 10 + num5;  //12 나 123 처럼 한자리 이상으로 넘어갈때를 작업 해주는 과정.
           				}
            			}
       			}
			Burndamage=number;
			Myteam->burn2(Burndamage,Yourteam,anyName);
			if((Myteam->indiPrint())==1)
			{
				for(int i=0; i<(Myteam->points());i++)
				{
					if((Yourteam->checkblank())==1)
					{
						if(Yourteam->_Name(i)!="")
							cout<<infoLog(Myteam,i)<<" / "<<infoLog(Yourteam,i)<<endl;
						else
							cout<<infoLog(Myteam,i)<<endl;
					}
					else
					{
						cout<<infoLog(Myteam,i)<<endl;
					}	
				}
			}
			Myteam->resetPrint();
		}
			
	}
    
    	return true; // 이상한거 입력 안되면 true 나오게 되어있음.
}

int main()
{	
	Creative_stone* _Myteam=new Creative_stone();       //아군
	Creative_stone* _Yourteam=new Creative_stone();      //적군
	while(creative(_Myteam,_Yourteam)){ }
	return 0;
}
		
