#include <stdio.h>
#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <map>
#include "creative_stone.h"


using namespace std;

int Creative_stone::hp(int _point) const
{
	return Hp[_point];
}

int Creative_stone::power(int _point) const
{
	return Power[_point];
}

string Creative_stone::_Name(int _point)
{
	return stone[_point];
}

int Creative_stone::points() const
{
	return point;
}

int Creative_stone::indiPrint() const
{
	return printt;
}

void Creative_stone::resetPrint()
{
	printt=0;
}

void Creative_stone::minuspoint()
{
	point-=1;
}

int Creative_stone::checkblank()
{
	if(stone[0]!="")
		return 1;
	else
		return 0;
}

void Creative_stone::pushblank(int blank)
{
	string tmp;
	for(int i=blank;i<point;i++)
	{
		
		tmp=stone[i];
		stone[i]=stone[i+1];
		stone[i+1]=tmp;
	}	
}

void Creative_stone::eraseHp(int where)  //체력 데이터를 지워버림.
{
	int tmp;
	Hp[where]=0;
	if(where!=point-1)
	{
		for(int i=where;i<point;i++)
		{
			
			tmp=Hp[i];
			Hp[i]=Hp[i+1];
			Hp[i+1]=tmp;
		}
	}
	else 
		Hp[where]=0;	
}

void Creative_stone::erasePower(int where) //파워 데이터를 지워버림.
{
	int tmp;
	Power[where]=0;
	if(where!=point-1)
	{
		for(int i=where;i<point;i++)
		{
			
			tmp=Power[i];
			Power[i]=Power[i+1];
			Power[i+1]=tmp;
		}
	}
	else 
		Power[where]=0;	
}

Creative_stone::Creative_stone()
{
	Hp.reserve(10);  //체력들 나타내는 벡터
	for(int i=0;i<10;i++)
		Hp[i]=0;
   	Power.reserve(10);  //공격력 나타내는 벡터
	for(int i=0;i<10;i++)
		Power[i]=0;
	point=0;
	printt=0;
}

Creative_stone::~Creative_stone()
{
}

void Creative_stone::Setting(string _Name,int _hp, int _power)
{
	int check=0; //같은게 있는지 없는지.
	int where;   //어디에 같은게 있는지.
	//_Name과 같은 이름이 map상에 있는지 없는지 확인.
	for(int i=0; i<point+1; i++)
	{
		if(stone[i]==_Name)
		{
			check+=1;
			where=i;
			break;
		}
	}
	if(check==0) //_Name과 같은 이름이 없다면.
	{
		stone[point]=_Name;   //맵의 point번째는 _Name이 된다. 
		Hp[point]=_hp;
		Power[point]=_power;
		point+=1;
		printt=1;
	}
	else     //_Name과 같은 이름이 있다면 그거에 맞는 Hp와 Power을 다시 셋팅.
	{
		Hp[where]+=_hp;
		Power[where]+=_power;
		printt=1;
	}
			
}


    
void Creative_stone::attack(string _M1,Creative_stone *_enemy,string _M2) //attack 명령 수행
{
	int check=0;
	int where1=0;
	int where2=0;
	//_M1 _M2 가 각각 map에 있는 이름인지 확인하는 단계
	for(int i=0; i<point+1; i++)
	{
		if(stone[i]==_M1)
		{
			check+=1;
			where1=i;
			break;
		}
	}

	for(int i=0; i<point+1; i++)
	{
		if((_enemy->stone[i])==_M2)
		{
			check+=1;
			where2=i;
			break;
		}
	}

	if(check==2)  //_M1, _M2 둘다 각각 map에 있을때.
	{
		(_enemy->Hp[where2])-=Power[where1];
		Hp[where1]-=(_enemy->Power[where2]);
		if((_enemy->Hp[where2])<=0)    //적의 체력이 0이하가 되었다면
		{
			if(where2!=(_enemy->points()))
			{
				(_enemy->stone[where2])="";
				_enemy->pushblank(where2);
				_enemy->eraseHp(where2);
				_enemy->erasePower(where2);
				_enemy->minuspoint();
			}
			else
			{
				(_enemy->stone[where2])="";
				_enemy->eraseHp(where2);
				_enemy->erasePower(where2);
				_enemy->minuspoint();
			}
				
		}

		if((Hp[where1])<=0)    //아군의 체력이 0이하가 되었다면
		{
			if(where1!=points())
			{
				stone[where1]="";
				pushblank(where1);
				eraseHp(where1);
				erasePower(where1);
				minuspoint();
			}
			else
			{
				stone[where1]="";
				eraseHp(where1);
				erasePower(where1);
				minuspoint();
			}
				
		}
		printt=1;    //<-확인
		check=0;
	}
	else
		cout<<"CANNOT FIND MINNION"<<endl;		
}

void Creative_stone::burn1(int dam,Creative_stone *_enemy) //burn1 명령 수행
{
	for(int i=0; i<(_enemy->points());i++)
	{
		(_enemy->Hp[i])-=dam;
	}
	for(int i=0; i<_enemy->points();i++)
	{
		if((_enemy->Hp[i])<=0)    //적의 체력이 0이하가 되었다면
		{
			if(i!=(_enemy->points())-1)
			{
				(_enemy->stone[i])="";
				_enemy->pushblank(i);
				_enemy->eraseHp(i);
				_enemy->erasePower(i);
				(_enemy->point)-=1;
				i--;
			}
			else
			{
				(_enemy->stone[i])="";
				_enemy->eraseHp(i);
				_enemy->erasePower(i);
				(_enemy->point)-=1;
			}
		}
	}
	printt=1;
}

void Creative_stone::burn2(int dam2,Creative_stone *_enemy,string _Name) //burn2 명령 수행
{
	int check1=0;    //아군에 _Name이 있는지 확인
	int check2=0;    //적군에 _Name이 있는지 확인
	int where1=0;
	int where2=0;
	string Left;
	string Right;
	char c;
	int characnumber;
	for(int i=0; i<point+1; i++)
	{
		Left=stone[i];
		Right=_Name;
		for(int j=0;j<Left.length();j++)
		{
			if((int)(Left[j])>=65&&(int)(Left[j])<=90)
			{
				c=Left[j];
				characnumber=(int)c+32;
				c=(char)characnumber;
				Left[j]=c;
			}
		}
		for(int j=0;j<Right.length();j++)
		{
			if((int)(Right[j])>=65&&(int)(Right[j])<=90)
			{
				c=Right[j];
				characnumber=(int)c+32;
				c=(char)characnumber;
				Left[j]=c;
			}
		}

		if(Left==Right)
		{
			check1+=1;
			where1=i;
			break;
		}
	}

	for(int i=0; i<point+1; i++)
	{
		if((_enemy->stone[i])==_Name)
		{
			check2+=1;
			where2=i;
			break;
		}
	}
	if(check1==1&&check2==1)
	{
		(_enemy->Hp[where2])-=dam2;
		if((_enemy->Hp[where2])<=0)    //적의 체력이 0이하가 되었다면
		{
			if(where2!=(_enemy->points()))
			{
				(_enemy->stone[where2])="";
				_enemy->pushblank(where2);
				_enemy->eraseHp(where2);
				_enemy->erasePower(where2);
				_enemy->minuspoint();
			}
			else
			{
				(_enemy->stone[where2])="";
				_enemy->eraseHp(where2);
				_enemy->erasePower(where2);
				_enemy->minuspoint();
			}
				
		}
		Hp[where1]-=dam2;
		if(Hp[where1]<=0)    //아군의 체력이 0이하가 되었다면
		{
			if(where1!=points())
			{
				stone[where1]="";
				pushblank(where1);
				eraseHp(where1);
				erasePower(where1);
				minuspoint();
			}
			else
			{
				stone[where1]="";
				_enemy->eraseHp(where1);
				_enemy->erasePower(where1);
				_enemy->minuspoint();
			}
				
		}
		printt=1;
		
	}
	else if(check1==1)
	{
		Hp[where1]-=dam2;
		if(Hp[where1]<=0)    //아군의 체력이 0이하가 되었다면
		{
			if(where1!=points())
			{
				stone[where1]="";
				pushblank(where1);
				eraseHp(where1);
				erasePower(where1);
				minuspoint();
			}
			else
			{
				stone[where1]="";
				_enemy->eraseHp(where1);
				_enemy->erasePower(where1);
				_enemy->minuspoint();
			}
				
		}
		printt=1;
	}
	else if(check2==1)
	{
		(_enemy->Hp[where2])-=dam2;
		if((_enemy->Hp[where2])<=0)    //적의 체력이 0이하가 되었다면
		{
			if(where2!=(_enemy->points()))
			{
				(_enemy->stone[where2])="";
				_enemy->pushblank(where2);
				_enemy->eraseHp(where2);
				_enemy->erasePower(where2);
				_enemy->minuspoint();
			}
			else
			{
				(_enemy->stone[where2])="";
				_enemy->eraseHp(where2);
				_enemy->erasePower(where2);
				_enemy->minuspoint();
			}
				
		}
		printt=1;
	}
	else
	{
		cout<<"CANNOT FIND MINNION"<<endl;
	}
	check1=0;
	check2=0;
}
