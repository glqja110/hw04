#include <iostream>
#include <stdlib.h>

using namespace std;


int main(void)
{
	while(true)
	{
 	       string inputs;  //inputs 라는 string에 이제 입력할꺼임.
 	       getline(cin, inputs);  //inputs에 입력.
 	       
 	       string *letter = new string[inputs.length()]; // more than count  letter라는 string 집합을 만듦. 
 	       string token="";  
 	       int argc = 0;  //argc는 숫자, 문자들의 갯수를 가리키는 변수.
		int check=0; 
		int blank=0;  //공백 수 고려.
		int check2=0; //두자릿수였던 횟수.
		for(int i=0;i<inputs.length();i++)    //빈 공간은 무시하고 문자들을 letter배열에 넣을거임. 
		{
			if(inputs[i]!=' ')
			{
				if(i!=inputs.length()-1)   //i가 맨 끝이 아닐경우 (두자리 이상의 수들을 고려하기 위해)
				{
					if(inputs[i+1]!=' ')    //그 다음 문자가 공백이 아니면 (두자릿 수 이상이라는 소리)
					{
						token=token+inputs[i];  //token에 우선 저장해 둠. 다음 공백을 만나면 그때 이 token에 있는걸 붙일것임.
						check+=1;  //두자릿 수 이상인지 확인.
						check2+=1;
					}
	
					else //그 다음이 공백이면
					{
						if(check>0)  //저번것이 공백이 아니라서 check가 하나 올랐을때
						{
							letter[i-check2-blank]=token+inputs[i]; //저장된 token에 붙임 
							token="";  //token은 다시 아무것도 없는 상태로 됨
							check=0;  //다음 체크를 위해.
						}
						else  //그냥 다음 공백이고 두 자릿수도 아니면
							letter[i-blank-check2]=inputs[i];  //그냥 배열에 추가
					}
						
				}
			
				
				else if(i==inputs.length()-1)  //맨 끝 상황
				{
					if(check>0) //이미 끝까지 왔는데 두자릿수 이상이 예상된다면
					{
						letter[i-blank-check2]=token+inputs[i];  //token에 저장된거 붙여서 letter에 저장
						//argc=i-blank-check2+1;
					}
					else //맨 끝까지 왔는데 걍 한자리 수라면
					{
						letter[i-blank-check2]=inputs[i];
						//argc=i-blank-check2+1;
					}
				}
			}
			else if(inputs[i]==' ')
				blank++;
		}
		int total=0;  //총 합이 몇인지 혹은 blackjack 이나 exceed 상태도 판단함.
		int count=0; //A가 몇개 있는지를 세기 위함
		string num;
		string numbers;
		int donot=0;
		char c;
		int check_count=0;
		numbers=letter[0];
		c=numbers[0];
		if(isdigit(c) == true)
		{
			argc=((int)(numbers[0]))-48;
		}
		else 	
			return false;

		for(int i=1; i<argc+1; i++)
		{
			if(letter[i]=="1"||letter[i]=="2"||letter[i]=="3"||letter[i]=="4"||letter[i]=="5"||letter[i]=="6"||letter[i]=="7"||letter[i]=="8"||letter[i]=="9"||letter[i]=="10"||letter[i]=="A"||letter[i]=="J"||letter[i]=="Q"||letter[i]=="K")
			{
			if(letter[i]=="1"||letter[i]=="2"||letter[i]=="3"||letter[i]=="4"||letter[i]=="5"||letter[i]=="6"||letter[i]=="7"||letter[i]=="8"||letter[i]=="9"||letter[i]=="10")
			{
				if(letter[i]!="10")
				{
					num=letter[i];
					total+=((int)(num[0]))-48;
				}
				else if(letter[i]=="10")
					total+=10;
			}

			else if(letter[i]=="K"||letter[i]=="J"||letter[i]=="Q")
			{
				total+=10;
			}
			else if(letter[i]=="A")
				count++;
			}
			else
				donot=1;
		}
		if(donot==0)
		{
			if(count>0)
			{
				if(total<10)
				{
					if(total==9)     //9 A A = 9+1+11=21
					{
						if(count==2)
							cout<<"BlackJack"<<endl;
					}

					else
					{
						total+=11;
						count-=1;
						if(count>0)
							total+=count;
						if(total<21)
							cout<<total<<endl;
						else if(total==21)
							cout<<"BlackJack"<<endl;
						else
							cout<<"Exceed"<<endl;
					}	
				}
		
				else if(total<=21)
				{
					if(total==10)
					{
						if(count==1)
							cout<<"BlackJack"<<endl;
					}

					else
					{
						total+=count;
						if(total>21)
							cout<<"Exceed"<<endl;
						else if(total==21)
							cout<<"BlackJack"<<endl;
						else
							cout<<total<<endl;
					}
				}

				else if(total>21)
					cout<<"Exceed"<<endl;
			}

			else
			{
				if(total<21)
					cout<<total<<endl;
				else if(total==21) 
					cout<<"BlackJack"<<endl;
				else 
					cout<<"Exceed"<<endl;
			}
		}
		else if(donot==1)
				return false;

	}
	return 0;
}

