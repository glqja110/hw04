#ifndef creative_stone_h
#define creative_stone_h

#include <stdio.h>
#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <map>
#define MAX_SIZE 100

using namespace std;

class Creative_stone
{
private:
    vector<int> Hp;  //체력들 나타내는 배열
    vector<int> Power;  //공격력 나타내는 배열
    map<int,string> stone;  //stone 이란 이름의 map string에는 몬스터 이름이 int는 몇번째 앤지를 나타내어 체력과 공격력을 나타내는 배열에 그 데이터를 저장할것임.
    int point;    //몇번째 애인질 가리킴.
    int printt;   //print를 할지 말지.
    
public:
    Creative_stone(); //생성자, 체력 0, 공격력 0, 상태 Invalid로 초기화
    ~Creative_stone();
    void Setting(string _Name,int _hp, int _power);
    
    int hp(int _point) const;
    int power(int _point) const;
    int points() const;
    string _Name(int _point);
    int indiPrint() const;
    int checkblank();	
    
    void eraseHp(int where); //체력 데이터를 지워버림.
    void erasePower(int where); //파워 데이터를 지워버림.
    void resetPrint();
    void minuspoint();
    void pushblank(int blank);
    
    void attack(string _M1,Creative_stone *_enemy,string _M2); //attack 명령 수행
    void burn1(int dam,Creative_stone *_enemy); //burn1 (burn #) 명령 수행
    void burn2(int dam2,Creative_stone *_enemy,string _Name); //burn2 (burn name #) 명령 수행
};
#endif /* creative_stone_h */
