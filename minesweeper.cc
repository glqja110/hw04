#include <stdio.h>
#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include "minesweeper.h"

using namespace std;

Minesweeper::Minesweeper()
{
	t_ouchCount=0;
	w_idth=0;
	h_eight=0;
	mine.reserve(MAX_SIZE);  //mine vector 크기 설정해놓음.
	playmine.reserve(MAX_SIZE);
}

Minesweeper:: ~Minesweeper()
{
	
}

bool Minesweeper::setMap(size_t _width, size_t _height)      //_width X _height만큼 맵을 셋팅하는 과정.
{   
	w_idth=_width;
	h_eight=_height;
	for(int i=0;i<w_idth;i++)
		mine[i].resize(w_idth,"");      //mine vector 모두 빈공간으로 우선 초기화.

	string letter;
	for(int i=0;i<h_eight;i++)
	{
		getline(cin, letter);
		if(letter.length()!=w_idth)
			return false;
		else
		{
			for(int j=0;j<w_idth;j++)
			{
				if(letter[j]=='*'||letter[j]=='.')         //*이나 .를 입력 하진 않았을 땐 false를 리턴. 
					mine[i][j]=letter[j];
				else
					return false;
			}
		}
	}
	
	//이제 지뢰위치와 숫자들을 셋팅하는 과정

	//지뢰는 냅두고 .으로 되었던 부분은 다 주변 지뢰 갯수만큼 표시하게 해놓음.
	for(int i=0;i<h_eight;i++)
	{
		for(int j=0;j<w_idth;j++)
		{
			cout<<get(j,i);

		}
		cout<<endl;
	}
}

bool Minesweeper::toggleMine(int _x, int _y)
{
	if(mine[_y][_x]=="*")
		mine[_y][_x]=".";
	else
		mine[_y][_x]="*";
	for(int i=0;i<h_eight;i++)
	{
		for(int j=0;j<w_idth;j++)
		{
			cout<<get(j,i);

		}
		cout<<endl;
	}
}

size_t Minesweeper::width() const
{
	return w_idth;
}

size_t Minesweeper::height() const
{
	return h_eight;
}

char Minesweeper::get(int _x, int _y) const
{
	int count; //지뢰 갯수 세는 변수.
	count=0;
	if(mine[_y][_x]!="*")
	{
			
		if(_y+1<h_eight&&_y-1>=0&&_x+1<w_idth&&_x-1>=0)     //8방향 모두 vector크기상 문제가 없을때
		{
			for(int k=_y-1;k<=_y+1;k++)
			{
				for(int m=_x-1;m<=_x+1;m++)
				{
					if(mine[k][m]=="*")
						count++;
				}
			}
			return (char)(count+48);      //mine[i][j]는 8방향 지뢰 갯수만큼으로 배정됨.
		}
			
		else if(_y-1<0&&_x-1<0)    //위 왼쪽이 vector 크기상 문제가 있을때.
		{
			for(int k=_y;k<=_y+1;k++)
			{
				for(int m=_x;m<=_x+1;m++)
				{
					if(mine[k][m]=="*")
						count++;
				}
			}
			return (char)(count+48);      //mine[i][j]는 8방향 지뢰 갯수만큼으로 배정됨.
		}
		
		else if(_y+1>=h_eight&&_x-1<0)    //아래 왼쪽이 vector 크기상 문제가 있을때.
		{
			for(int k=_y-1;k<=_y;k++)
			{
				for(int m=_x;m<=_x+1;m++)
				{
					if(mine[k][m]=="*")
						count++;
				}
			}
			return (char)(count+48);      //mine[i][j]는 8방향 지뢰 갯수만큼으로 배정됨.
		}	
	
		else if(_y-1<0&&_x+1>=w_idth)    //위 오른쪽이 vector 크기상 문제가 있을때.
		{
			for(int k=_y;k<=_y+1;k++)
			{
				for(int m=_x-1;m<=_x;m++)
				{
					if(mine[k][m]=="*")
						count++;
				}
			}
			return (char)(count+48);      //mine[i][j]는 8방향 지뢰 갯수만큼으로 배정됨.
		}	
			
		else if(_y+1>=h_eight&&_x+1>=w_idth)    //아래 오른쪽이 vector 크기상 문제가 있을때.
		{
			for(int k=_y-1;k<=_y;k++)
			{
				for(int m=_x-1;m<=_x;m++)
				{
					if(mine[k][m]=="*")
						count++;
				}
			}
			return (char)(count+48);      //mine[i][j]는 8방향 지뢰 갯수만큼으로 배정됨.
		}	
		else if(_y-1<0)     //위가 vector 크기상 문제가 있을때
		{
			for(int k=_y;k<=_y+1;k++)
			{
				for(int m=_x-1;m<=_x+1;m++)
				{
					if(mine[k][m]=="*")
						count++;
				}
			}
			return (char)(count+48);      //mine[i][j]는 8방향 지뢰 갯수만큼으로 배정됨.
		}	
		else if(_y+1>=h_eight)     //아래가 vector크기상 문제가 있을때
		{
			for(int k=_y-1;k<=_y;k++)
			{
				for(int m=_x-1;m<=_x+1;m++)
				{
					if(mine[k][m]=="*")
						count++;
				}
			}
			return (char)(count+48);      //mine[i][j]는 8방향 지뢰 갯수만큼으로 배정됨.
		}	
		else if(_x-1<0)   //왼쪽이 vector크기상 문제가 있을때
		{
			for(int k=_y-1;k<=_y+1;k++)
			{
				for(int m=_x;m<=_x+1;m++)
				{
					if(mine[k][m]=="*")
						count++;
				}
			}
			return (char)(count+48);      //mine[i][j]는 8방향 지뢰 갯수만큼으로 배정됨.
		}	
		else if(_x+1>=w_idth)   //오른쪽이 vector크기상 문제가 있을때
		{
			for(int k=_y-1;k<=_y+1;k++)
			{
				for(int m=_x-1;m<=_x;m++)
				{
					if(mine[k][m]=="*")
						count++;
				}
			}
			return (char)(count+48);      //mine[i][j]는 8방향 지뢰 갯수만큼으로 배정됨.
		}	
	}
	else if(mine[_y][_x]=="*")
		return '*';
	else
		return ' ';
}

bool Minesweeper::setPlay()
{
	for(int i=0;i<h_eight;i++)
		playmine[i].resize(w_idth,"_");      //mine vector 모두 빈공간으로 우선 초기화.

	for(int i=0;i<h_eight;i++)
	{
		for(int j=0;j<w_idth;j++)
		{
			cout<<playmine[i][j];
		}
		cout<<endl;
	}
}

bool Minesweeper::touchMap(int _x, int _y)
{
	//if(_x>=w_idth||_y>=h_eight) return false;

	if(mine[_y][_x]!="*")
	{
		playmine[_y][_x]=get(_y,_x);
		t_ouchCount++;

		for(int i=0;i<h_eight;i++)
		{
			for(int j=0;j<w_idth;j++)
			{
				cout<<playmine[i][j];
			}
			cout<<endl;
		}

	}
	else
	{
		cout<<"DEAD("<<touchCount()+1<<")"<<endl;
		t_ouchCount=0;
	}
		
	
		
}

int Minesweeper::touchCount() const
{
	return t_ouchCount;
}
