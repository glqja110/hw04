#include <iostream>
#include <string>
#include <stdlib.h>
#include <sstream>
#include <vector>
#include <map>
#include <list>
#include "reply_admin.h"

using namespace std;

ReplyAdmin::ReplyAdmin()
{
	addChat("Hello, Reply Administrator!");
	addChat("I will be a good programmer.");
 	addChat("This class is awesome.");
 	addChat("Professor Lim is wise.");
 	addChat("Two TAs are kind and helpful.");
 	addChat("I think male TA looks cool.");
}

ReplyAdmin::~ReplyAdmin()
{
}


bool ReplyAdmin::addChat(string _chat)
{
	string check;
	check+=_chat;
	if(_chat==check)  //_chat이 문자면
	{
		chats.push_back(_chat);
		return true;
	}  
	
}

bool ReplyAdmin::removeChat(int _index)
{
	if(_index<chats.size())    //_index가 chats의 갯수보다 작을때만
	{
		it=chats.begin();  //it은 chats의 맨 앞을 가리키고 있음.
		for(int i=0;i<_index;i++)       //체크
		{
			it++;
	
		}
		chats.erase(it);
	}
	else
		return false;
		
}

bool ReplyAdmin::removeChat(int *_indices, int _count)
{
	
	int tmp;
	//_indicies 배열 순서대로 정렬
	for(int i=0;i<_count-1;i++)
	{
		for(int j=0;j<_count-1;j++)
		{
			if(_indices[j]>_indices[j+1])
			{
				tmp=_indices[j];
				_indices[j]=_indices[j+1];
				_indices[j+1]=tmp;
			}
		}
	}
	int number;
	int count2=0;   //몇번 앞으로 땡겨야 하는지
	for(int i=0;i<_count;i++)
	{
		number=_indices[i];
		if(number<chats.size()+count2)
		{
			if(number!=chats.size()-1)    //땡길 필요가 있음
			{
				it=chats.begin();  //it은 chats의 맨 앞을 가리키고 있음.
				for(int i=0;i<number-count2;i++)       //체크
				{
					it++;
		
				}
				chats.erase(it);
				count2++;
			}
			else if(number==chats.size()-1+count2)
			{
				for(int i=0;i<number-count2;i++)       //체크
				{
					it++;
		
				}
				chats.erase(it);
			}	
		}
			
	}


}

bool ReplyAdmin::removeChat(int _start, int _end)
{

	int start=_start;
	int end=_end;
	if(start<chats.size())
	{
		if(end>=chats.size())
			end=chats.size()-1;
		it_low=chats.begin();
		for(int i=0;i<start;i++)
			it_low++;
		it_high=chats.begin();
		for(int i=0;i<end+1;i++)
			it_high++;
		chats.erase(it_low,it_high);
	}
		
		
}
void ReplyAdmin::printChat()
{

	int count = chats.size();
	int i=0;
	for(it=chats.begin(); it!=chats.end(); it++)
	{
		cout << i << " " << *it <<endl;
		i++;
	}
}
