// reply_admin.h
// implement your reply_admin.cc

// You should reuse reply_admin.cc in previous assignment,
// but you MUST NOT modify this header file.

#ifndef __hw03__reply_admin__
#define __hw03__reply_admin__

#define NUM_OF_CHAT 200

#include <iostream>
#include <string>
#include <stdlib.h>
#include <sstream>
#include <vector>
#include <map>
#include <list>
using namespace std;

class ReplyAdmin
{
private:

    list<string> chats;  //이걸로 바뀜.
    list<string>::iterator it;
    list<string>::iterator it_low;
    list<string>::iterator it_high;

    //int getChatCount(); //글자 수 세는거
    
public:
    ReplyAdmin();  //chats를 NUM_OF_CHAT만큼 초기화 (생성자)
    ~ReplyAdmin(); //chats를 delete (소멸자)
    
    bool addChat(string _chat);  // _chat을 chats에 추가 (추가 실패시 false 리턴)   //list<string> chats에 맞춰서.
    //list<string> chats에 맞춰서
    bool removeChat(int _index);                // #remove #  _index에 있는 chat 삭제 (_index가 없을시 fasle 리턴) 
    bool removeChat(int *_indices, int _count); // #remove #,#,#,#  _count 크기의 _indices 배열 안에 있는 index에 해당되는 chat을 모두 삭제(하나라도 삭제 성공했을 시 true 리턴, index가 없을 시 무시)
    bool removeChat(int _start, int _end);      // #remove #-#  _start부터 _end까지 chat을 모두 삭제 (하나라도 삭제 성공 시 true 리턴, start가 음수거나 end가 chats보다 큰 경우 해당되는 부분만 삭제)
    void printChat();
};

#endif

/********************* WARNING! ************************

You MUST add these following functions in ReplyAdmin():

 addChat("Hello, Reply Administrator!");
 addChat("I will be a good programmer.");
 addChat("This class is awesome.");
 addChat("Professor Lim is wise.");
 addChat("Two TAs are kind and helpful.");
 addChat("I think male TA looks cool.");

*******************************************************/

