#ifndef minesweeper_h
#define minesweeper_h

#include <stdio.h>
#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#define MAX_SIZE 100

using namespace std;

class Minesweeper
{
public:
    ////////// 4-3-1 (7 score) //////////
    Minesweeper();    //생성자
    ~Minesweeper();   //소멸자
    
    // return false when input is incorrect
    bool setMap(size_t _width, size_t _height);  //맵을 셋팅하는 함수. _width:가로 _height:높이 
    bool toggleMine(int _x, int _y);
    
    // return map width, height, and char
    size_t width() const;   //가로
    size_t height() const;   //세로
    char get(int _x, int _y) const; // return ' ' if input is illegal    이상한게 입력 되면 ' '를 리턴
    
    ////////// 4-3-2 (3 score) //////////
    bool setPlay(); // return false when map is not set     Play 하는 함수. 맵이 이상하게 리턴되면 false 리턴
    bool touchMap(int _x, int _y); // return true when dead    지뢰가 어디있는지 찾는 함수. 지뢰 건드리면 죽음
    
    int touchCount() const;     //touch한 횟수. 
    
private:
	int h_eight;
	int w_idth;
	int t_ouchCount;
	vector<vector<string> > mine;
	vector<vector<string> > playmine; 
	
};

#endif /* minesweeper_h */
