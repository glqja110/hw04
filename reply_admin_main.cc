#include <iostream>
#include <string>
#include <stdlib.h>
#include <sstream>
#include <vector>
#include <map>
#include <list>
#include "reply_admin.h"

using namespace std;

bool replyOperation(ReplyAdmin *_replyAdmin)
{
    string inputs;  //inputs라는 입력 하는 string 선언.
    getline(cin, inputs);
    
    if(inputs == "#quit") return false;  //#quit 입력하면 탈출.
    else if(inputs.find("#remove") != string::npos)
    {
        inputs.erase(0, 8);  //#remove 를 지움.
        
        int start = -1;
        int end = -1;
        int number = -1;
        
        int* numbers = new int[NUM_OF_CHAT];  //numbers 라는 int 형 배열 선언.
        int numbersCount = 0;   //numbers 배열의 갯수 가리키는 변수.
        
        bool isRemoveValid = false;
        
        for(int i=0; i<inputs.size(); ++i)
        {
            char c = inputs[i];
            
            if(isdigit(c) == true)
            {
                int num = (int)c - (int)'0';  //문자로 입력된 것들 숫자로 바꾸는 작업.
                if(number < 0) number = num; 
                else number = number * 10 + num;  //12 나 123 처럼 한자리 이상으로 넘어갈때를 작업 해주는 과정.
            }
            else
            {
                if(c == ',') numbers[numbersCount++] = number; //, 사이 에 있는 숫자들을 numbers라는 배열에 넣어주는 과정 (numbers)는 , 에서 쓰일 것임.
                else if(c == '-') start = number; // 숫자 - 숫자 에 쓰일 것.
                number = -1;
            }
        }
        
        if(start >= 0) end = number;  //숫자 - 숫자에 쓰일 것.
        
        if(end >= start && start >= 0) isRemoveValid = _replyAdmin->removeChat(start, end); // 숫자 - 숫자 경우.  
        else if(numbersCount > 0)  // 숫자, 숫자 .. 의 경우. 
        {
            numbers[numbersCount++] = number; // final number
            isRemoveValid = _replyAdmin->removeChat(numbers, numbersCount); 
        }
        else if(number >= 0) isRemoveValid = _replyAdmin->removeChat(number);
        
        if(isRemoveValid == true) _replyAdmin->printChat();  //출력
        
        delete[] numbers;
    }
    else if(_replyAdmin->addChat(inputs)) _replyAdmin->printChat(); //#remove 아니면 걍 문자 입력하기 그런다음 출력
    
    return true;
}

int main(void)
{
    ReplyAdmin *replyAdmin = new ReplyAdmin(); //replyAdmin이란 새로운 애 만듦.
    
    while(replyOperation(replyAdmin)) { /* Nothing else to do. */ } //false 아닌 이상 계속 입력 하는 과정.
    
    delete replyAdmin;
    return 0;
}

