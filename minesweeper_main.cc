#include <stdio.h>
#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include "minesweeper.h"

using namespace std;

bool mine_sweeper(Minesweeper* _mine)
{
	string inputs;  //inputs라는 입력 하는 string 선언.
    	getline(cin, inputs);
	int width = -1;
        int height = -1;
	int number=-1;

	int setting=-1;  //맵을 셋팅했는지를 나타내는 변수.
	if(inputs == ":quit") return false;  //:quit 입력하면 탈출.
 	else if(inputs.find(":set") != string::npos)    //:set width height
    	{
       		inputs.erase(0, 5);  //:set 을 지움.
                

        	for(int i=0; i<inputs.size(); ++i)
        	{
            		char c = inputs[i];
            
            		if(isdigit(c) == true)
            		{
                		int num = (int)c - (int)'0';  //문자로 입력된 것들 숫자로 바꾸는 작업.
                		if(number < 0) number = num; 
                		else number = number * 10 + num;  //12 나 123 처럼 한자리 이상으로 넘어갈때를 작업 해주는 과정.
            		}
			else
            		{
                		if(c == ' ') width = number; // 가로 길이 숫자를 정함
                		number = -1;
            		}
		}
		if(width >= 0) height = number;  //세로 길이 숫자를 정함
		//:set width height가 정해짐. 

		if(_mine->setMap(width,height)){ };  //map이여야 하는지 map(height)인지 확인
	}
	else if(inputs.find(":toggle") != string::npos)    //:toggle to_x to_y
	{
		int to_x;     
		int to_y;
		inputs.erase(0,8); //:toggle을 지움.
		
		for(int i=0; i<inputs.size(); ++i)
        	{
            		char c2 = inputs[i];
            
            		if(isdigit(c2) == true)
            		{
                		int num2 = (int)c2 - (int)'0';  //문자로 입력된 것들 숫자로 바꾸는 작업.
                		if(number < 0) number = num2; 
                		else number = number * 10 + num2;  //12 나 123 처럼 한자리 이상으로 넘어갈때를 작업 해주는 과정.
            		}
			else
            		{
                		if(c2 == ' ') to_x = number; // 가로 길이 숫자를 정함
                		number = -1;
            		}
		}
		if(to_x >= 0) to_y = number;  //세로 길이 숫자를 정함
		_mine->toggleMine(to_x,to_y);
		
		
	}

	else if(inputs.find(":play") != string::npos)   //:play
	{
		if((_mine->height())!=0)
		{
			_mine->setPlay();
		}
		else
			return false;
		
	}

	else if(inputs.find(":touch") != string::npos)  //:touch touch_x touch_y
	{
		int touch_x;     
		int touch_y;
		inputs.erase(0,7); //:touch을 지움.
		
		for(int i=0; i<inputs.size(); ++i)
        	{
            		char c3 = inputs[i];
            
            		if(isdigit(c3) == true)
            		{
                		int num3 = (int)c3 - (int)'0';  //문자로 입력된 것들 숫자로 바꾸는 작업.
                		if(number < 0) number = num3; 
                		else number = number * 10 + num3;  //12 나 123 처럼 한자리 이상으로 넘어갈때를 작업 해주는 과정.
            		}
			else
            		{
                		if(c3 == ' ') touch_x = number; // 가로 길이 숫자를 정함
                		number = -1;
            		}
		}
		if(touch_x >= 0) touch_y = number;  //세로 길이 숫자를 정함

		_mine->touchMap(touch_x,touch_y);

	}
	return true;
}

int main()
{
	Minesweeper* m_ine=new Minesweeper();
	while(mine_sweeper(m_ine)){ };
	delete m_ine;
	return 0;
}
		
